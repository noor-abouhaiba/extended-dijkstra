# Extended Dijkstra's Shortest Path 

This algorithm uses a HashGraph implemented using hash maps and hash sets, both of which use open addressing. This HashGraph also makes use of a class called LocalInfo as well as an adjustable heap priority queue to easily access the minimum cost used to achieve the shortest path. The LocalInfo class uses HashOpenSets (hash sets using open addressing) to store the out nodes and in nodes. The out nodes represent nodes with edges that lead to them directly from the current node (the origin) in the graph, and the in nodes represent nodes whose edges lead to this current node (destination). This LocalInfo class also contains hash sets of out edges and in edges. Out edges representing to edges leading to other nodes from the current node (origin node), whereas in edges represents edges from other nodes leading to this node (destination node).

An example of a LocalInfo object is similar to the following:
 
Info[destination-node,cost,origin-node]
<br/>e.g.<br/> A->Info[A,10,B]<br/>
say A is a destination node of B within the HashGraph, A has a LocalInfo object that contains the above information. A is its own destination node, A has an origin node, B, that can directly reach A with a cost (edge value) of 10.  

The user input given start node required to calculate the shortest path will always have an Info object similar to the following, this is because we consider the start node to be considered its own destination with no origin node:

Info[destination-node, 0, ?] 

The HashGraph class is a templated class that builds a graph from a text file as input with specific formatting for node input to build a directed graph as well as a separator character used to distinguish this data (/). The HashGraph structure is built using two separate sub-graphs consisting of nodes and edges. The node sub-graph maps a key, the node, to its own associated LocalInfo object which keeps track of four sets: the out/in nodes, destination nodes with the current node as its origin and destination respectively,  and the out/in edges, all edges with the current node as their origin node and all edges with this current node as its destination node, respectively. 

The algorithm makes use of an adjustable heap priority queue that utilizes a greater than function based on the cost of the edge between an origin node and all its possible destination nodes, the cost values are sorted and adjusted in the heap in ascending order. The priority queue contains a map of information regarding origin nodes and their Info objects. This heap does most of the heavy work of the algorithm, it is given a map of information and once a start node is given as input it begins to build a result map that contains the final shortest path between nodes within the graph. An example of this is as follows:

# Example
Consider a graph of airports input from a text file, the graph contains the following nodes and translates the text file's contents into a graph of the following nodes:
* BOS
* BWI
* DFW
* JFK
* LAX
* MIA
* ORD
* PVD
* SFO
* ORD 

The info map pulls from the text file's contents of edge values between out/in nodes and out/in edges that is now stored in memory and populates itself with the following (in sorted, ascending order by cost, and if costs are equal then it is sorted by ascending order of node names):<br/>
<p align="center">
"BOS"&nbsp; -> ["BOS", INT_MAX, "?"]<br/>
"BWI"&nbsp; -> ["BWI", INT_MAX, "?"]<br/>
"DFW"&nbsp; -> ["DFW", INT_MAX, "?"]<br/>
"JFK"&nbsp; -> ["JFK", INT_MAX, "?"]<br/>
"LAX"&nbsp; -> ["LAX", INT_MAX, "?"]<br/>
"MIA"&nbsp; -> ["MIA", INT_MAX, "?"]<br/>
"ORD"&nbsp; -> ["ORD", INT_MAX, "?"]<br/>
"PVD"&nbsp; -> ["PVD", INT_MAX, "?"]<br/>
"SFO"&nbsp; -> ["SFO", INT_MAX, "?"]<br/>
</p>
<br/>Now, assume user input is given for the start node to compute the shortest path from and that node is BWI, the priority queue updates  "BWI" -> ["BWI", INT_MAX, "?"] to  "BWI" -> ["BWI", 0, "?"], meaning that BWI, with respect to the start node (BWI) has a cost of 0 and a destination that is itself with an undefined origin node. The priority queue is then able to adjust itself after this modification because of its nature as a heap and now the info map is updated to become:<br/>
<p align="center">
"BWI"&nbsp; -> ["BWI", &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 0, "?"]<br/>
"BOS"&nbsp; -> ["BOS", INT_MAX, "?"]<br/>
"DFW"&nbsp; -> ["DFW", INT_MAX, "?"]<br/>
"JFK"&nbsp; -> ["JFK", INT_MAX, "?"]<br/>
"LAX"&nbsp; -> ["LAX", INT_MAX, "?"]<br/>
"MIA"&nbsp; -> ["MIA", INT_MAX, "?"]<br/>
"ORD"&nbsp; -> ["ORD", INT_MAX, "?"]<br/>
"PVD"&nbsp; -> ["PVD", INT_MAX, "?"]<br/>
"SFO"&nbsp; -> ["SFO", INT_MAX, "?"]<br/>
</p>
<br/>Afterwards, the minimum cost is found from the priority queue (in this case BWI with a cost of 0) and this node is removed from the priority queue and inserted into the result map. Now, the destination nodes of the newly inserted node are updated with a value of the edge cost from this node. Consider BWI has destination nodes JFK, MIA, and ORD. Since there is no minimum cost found for these nodes (is easily looked up by checking if any of these nodes are contained in the result map), because of this all of these destination nodes will be processed. Now, the info for each of these destination nodes is pulled from the info map and the cost to reach BWI (0) in the answer map is summed with the cost from the graph between BWI and these destination nodes. If the sum is smaller than the current value in the info map (this will be true since they are all INT_MAX) the info_map will be updated to the smaller value and as a result the origin node for these destination nodes will be updated to match the most recently added node in the result map (BWI in this case). The outcome is the following: <br/>
<p align="center">
"JFK"&nbsp; -> ["JFK", &nbsp; 184, "BWI"]<br/>
"ORD"&nbsp; -> ["ORD", &nbsp; 621, "BWI"]<br/>
"MIA"&nbsp; -> ["MIA", &nbsp; 946, "BWI"]<br/>
"BOS"&nbsp; -> ["BOS", INT_MAX, "?"]<br/>
"DFW"&nbsp; -> ["DFW", INT_MAX, "?"]<br/>
"LAX"&nbsp; -> ["LAX", INT_MAX, "?"]<br/>
"PVD"&nbsp; -> ["PVD", INT_MAX, "?"]<br/>
"SFO"&nbsp; -> ["SFO", INT_MAX, "?"]<br/>
</p>
<br/>Now, the minimum cost is removed from the priority queue and is added to the result map (in this case JFK). Now, the most recently added node to the result map (JFK) has its destination nodes checked and updated in the priority queue if the new proposed cost is smaller than the current cost. The destination nodes of JFK are BOS, BWI, DFW, ORD, and PVD. BWI is already in the result map so it will not be processed or updated. The others are still in the info map so they will all have their current cost in the info map compared to the cost it takes to reach JFK from the start node (184) summed with the cost from the graph between JFK and any of these destination nodes and if the summed cost is less than the current cost, the cost will be updated. The result is: <br/>
<p align="center">
"PVD"&nbsp; -> ["PVD", &nbsp; 328, "JFK"]<br/>
"BOS"&nbsp; -> ["BOS", &nbsp; 371, "JFK"]<br/>
"ORD"&nbsp; -> ["ORD", &nbsp; 621, "BWI"]<br/>
"MIA"&nbsp; -> ["MIA", &nbsp; 946, "BWI"]<br/>
"DFW"&nbsp; -> ["DFW", &nbsp;1575, "JFK"]<br/>
"LAX"&nbsp; -> ["LAX", INT_MAX, "?"]<br/>
"SFO"&nbsp; -> ["SFO", INT_MAX, "?"]<br/>
</p>
<br/>The minimum cost in the priority queue (PVD) is then removed and added to the result map. Then, the destination nodes of PVD are collected and if they are not already in the result map their current cost will be compared to the cost it takes to reach PVD (328) summed with the cost of the graph between PVD and its destination nodes, if the cost is less than the current cost in the info map then the costs and origin nodes will be updated, the priority queue will be adjusted, and one node will be removed from it and added to the result map. This will continue until the priority queue is entirely empty and the result map is the size of the initial info map. Then, the result map will contain the shortest path to all nodes within the graph from the given start node based on user input. 

# User Interface
The user is prompted to enter the name of a text file that represents the intended graph (samples are included) and displays the nodes, in ascending order, and each node's LocalInfo object storing out/in nodes and out/in edges. Once a start node is input, all reachable nodes are displayed and printed to the user as well as the shortest path of between the start node and all of its reachable nodes. The user is then prompted to enter a stop node. The shortest path is then pulled from the result map and the total cost as well as the shortest path to between start and end node is displayed to the user. The user can enter multiple stop nodes to have the shortest path between the start node and each additional stop node displayed. This continues indefinitely until the user exits. 

# Creating Input
It is possible to create your own text file to build a graph of its contents. Within your copied repository (information on that under the following section) there is a folder called input files within the extended-dijkstra folder, these can be used as reference, but here is an in depth explanation of how the information is parsed and its intended form. The text file is required to follow these guidelines:
the initial lines in the text file will contain just the name of each node on its own separate line\
e.g. <br/> 
* a
* b
* c <br/>
the line below the final node of this list is where you can begin defining your edge values of your directed graph between an origin and destination node. The required syntax is as follows:
an origin node, a destination node, and the cost value (each set of 3 is on its own separate line and each of these values is separated from the next with a '/' character)
<br/> e.g. <br/>
* a/b/3 *(translates to a->3->b, directed edge of cost 3 from node a to node b)*
* b/c/5
  
Once you have created your input text file you should save this file as (~.txt) and place it in the cmake-build-debug folder (~extended-dijkstra/cmake-build-debug). Once this is complete, you can begin running the executable and input your text file name when prompted to.
 
## Getting Started
To begin running this algorithm, Git is required to get a copy of this repository. To get a copy of this repository copy and paste into your command line: 
```bash
# Clone this 'Extended Dijkstra's Shortest Path algorithm' repository
$ git clone https://gitlab.com/noor-abouhaiba/extended-dijkstra.git
```
* Navigate to the **cmake-build-debug** directory in your command line 
* run the executable using **./program5**

# Demo
Below is a demo of how to clone this repository and run the executable. It is important that these steps are taken, otherwise you may encounter issues when trying to read the text file.

<img src="e_dijkstra.gif" alt = "Extended Dijkstra's">
