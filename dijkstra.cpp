#include <string>
#include <iostream>
#include <fstream>
#include "array_queue.hpp"
#include "hash_graph.hpp"
#include "dijkstra.hpp"



std::string get_node_in_graph(const ics::DistGraph& g, std::string prompt, bool allow_QUIT) {
  std::string node;
  for(;;) {
    node = ics::prompt_string(prompt + " (must be in graph" + (allow_QUIT ? " or QUIT" : "") + ")");
    if ( (allow_QUIT && node == "QUIT") || g.has_node(node) )
      break;
  }
  return node;
}

int main() {
  try {
      std::ifstream file;
      std::string start_node;
      std::string stop_node;
      ics::CostMap answer_map;
      ics::ArrayQueue<std::string> recovered_path;

      ics::safe_open(file, "Enter graph file name","flightcost.txt");
      ics::DistGraph graph;
      graph.load(file, ";");
      std::cout << graph << std::endl;

      start_node = get_node_in_graph(graph, "Enter start node", true);
      if(start_node == "QUIT")
          return 0;
      answer_map = extended_dijkstra(graph,start_node);
      std::cout << answer_map << std::endl;

      while (true) {
          stop_node = get_node_in_graph(graph, "Enter stop node", true);
          if (stop_node != "QUIT") {
              recovered_path = recover_path(answer_map, stop_node);
              std::cout << " path is " << recovered_path << std::endl << std::endl;
          }
          else
              break;
      }



  } catch (ics::IcsError& e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
