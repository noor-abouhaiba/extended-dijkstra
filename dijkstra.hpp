#ifndef DIJKSTRA_HPP_
#define DIJKSTRA_HPP_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>                    //Biggest int: std::numeric_limits<int>::max()
#include "array_queue.hpp"
#include "array_stack.hpp"
#include "heap_adjustable_hash_priority_queue.hpp"
#include "hash_graph.hpp"


namespace ics {

class Info {
    public:
        Info() { }

        Info(std::string a_node) : node(a_node) { }

        bool operator==(const Info &rhs) const { return node == rhs.node &&
                                                        cost == rhs.cost && from == rhs.from; }

        bool operator!=(const Info &rhs) const { return !(*this == rhs); }

        friend std::ostream &operator<<(std::ostream &outs, const Info &i) {
            outs << "Info[" << i.node << "," << i.cost << "," << i.from << "]";
            return outs;
        }

        //Public instance variable definitions
        std::string node = "?";
        int cost = std::numeric_limits<int>::max();
        std::string from = "?";
};


  bool gt_info(const Info &a, const Info &b) { return a.cost < b.cost; }
  //Put needed hashing functions here
  static int hash_info(const Info& s) {
      std::hash<std::string> str_hash;
      return str_hash(s.from);
  }
  typedef ics::HashGraph<int>                                               DistGraph;
  typedef ics::HeapAdjustableHashPriorityQueue<Info, gt_info,hash_info>     CostPQ;
  typedef ics::HashOpenMap<std::string, Info, DistGraph::hash_str>          CostMap;
  typedef ics::pair<std::string, Info>                                      CostMapEntry;

//Return the final_map as specified in the lecture-note description of
//  extended Dijkstra algorithm
  CostMap extended_dijkstra(const DistGraph &g, std::string start_node) {
      CostMap answer_map;
      CostMap info_map;
      CostPQ info_pq;
      CostMapEntry shortest;
      std::string min_node;
      int min_cost;
      HashGraph<int>::EdgeSet reachable;

      for (const auto& kv : g.all_nodes())
          info_map.put(kv.first, Info(kv.first));

      info_map[start_node].cost = 0;

//      std::cout << info_map << std::endl;

      for (const auto& info : info_map)
          info_pq.enqueue(info.second);
//      std::cout << info_pq << std::endl;

      while(!info_map.empty()) {
          shortest.second = info_pq.dequeue();

          shortest.first = shortest.second.node;
          min_cost = shortest.second.cost;
          min_node = shortest.second.node;
          if (min_cost == std::numeric_limits<int>::max())
              return answer_map;

          info_map.erase(shortest.first);
          answer_map.put(shortest.first, shortest.second);
//          std::cout << "answer map: " << answer_map << std::endl;

          reachable = g.out_edges(shortest.first);
          for (auto & kv : reachable) {
//              std::cout << min_node << " reachable " << kv << std::endl;
              if(!answer_map.has_key(kv.second)) {
                  if(info_map[kv.second].cost == std::numeric_limits<int>::max() || info_map[kv.second].cost > min_cost + g.edge_value(min_node, kv.second)) {
//                      std::cout << "info_map: " << info_map << std::endl;
//                      std::cout << "info_pq: " << info_pq << std::endl;
                      Info new_info;
                      new_info.node = kv.second;
                      new_info.from = min_node;
                      new_info.cost = g.edge_value(min_node, kv.second) + min_cost;

//                      std::cout << "old element: " << kv.second << std::endl;
//                      std::cout << "new element: " << new_info << std::endl;
                      info_pq.update(info_map[kv.second], new_info);
                      info_map[kv.second].cost = new_info.cost;
                      info_map[kv.second].from = new_info.from;
                  }
              }
          }
      }
        return answer_map;
  }


//Return a queue whose front is the start node (implicit in answer_map) and whose
//  rear is the end node
  ArrayQueue <std::string> recover_path(const CostMap &answer_map, std::string end_node) {
      ics::ArrayStack<Info> stack;
      ics::ArrayQueue<std::string> path;
      std::string origin = end_node;

      stack.push(answer_map[end_node]);

      while (true) {
          if (answer_map[origin].from != "?") {
              origin = answer_map[origin].from;
              stack.push(answer_map[origin]);
          }
          else
              break;
      }
      while (!stack.empty()) {
          Info i = stack.pop();
          path.enqueue(i.node);
      }
      std::cout << "Cost is " << answer_map[end_node].cost << ";";
      return path;
  }


}

#endif /* DIJKSTRA_HPP_ */
